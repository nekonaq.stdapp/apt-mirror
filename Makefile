SHELL = /bin/bash

run		= $(nil)

MAKE_ENVFILE	= .env
ifneq ($(wildcard $(MAKE_ENVFILE)),)
# ifeq ($(MAKELEVEL),0)
# $(info '## Makefile using envfile: $(MAKE_ENVFILE)')
# endif
include $(MAKE_ENVFILE)
SERVICE		= $(COMPOSE_PROJECT_NAME)
else
SERVICE		= $(notdir $(PWD))
endif

DESTDIR		=
INSTALL_OWNER	= root
INSTALL_GROUP	= root

SERVICE_PREFIX	= docker-compose@
SYSCONFDIR	= $(DESTDIR)/etc/default
UNITDIR_LIB	= $(DESTDIR)/lib/systemd/system
UNITDIR_ETC	= $(DESTDIR)/etc/systemd/system

SERVICE_OVERRIDE	= service.override.conf
ifeq (,$(wildcard $(SERVICE_OVERRIDE)))
SERVICE_OVERRIDE_FILE	= $(nil)
else
# カレントディレクトリに service.override.conf があれば仕込む
SERVICE_OVERRIDE_FILE	= $(UNITDIR_ETC)/$(SERVICE_PREFIX)$(SERVICE).service.d/$(SERVICE_OVERRIDE)
endif

SERVICE_TIMER		= service.timer
ifeq (,$(wildcard $(SERVICE_TIMER)))
SERVICE_TIMER_FILE	= $(nil)
else
# カレントディレクトリに service.timer があれば仕込む
SERVICE_TIMER_FILE	= $(UNITDIR_ETC)/$(SERVICE_PREFIX)$(SERVICE).timer
endif

all:; $(error No target specified)

#//
install remove name status start stop enable disable:
	@$(MAKE) --no-print-directory $(SERVICE_PREFIX)$(SERVICE).service/$@

ifdef SERVICE_TIMER_FILE
status.timer start.timer stop.timer enable.timer disable.timer:
	@$(MAKE) --no-print-directory $(SERVICE_PREFIX)$(SERVICE).timer/$(@:.timer=)
endif

#//
$(addsuffix .service/install,$(SERVICE_PREFIX)$(SERVICE)): %/install: $(SYSCONFDIR)/% $(SERVICE_OVERRIDE_FILE) $(SERVICE_TIMER_FILE) FORCE
	@( set -x; $(run) systemctl daemon-reload )

$(addsuffix .service/remove,$(SERVICE_PREFIX)$(SERVICE)): %/remove: FORCE
	@( set -x; \
	   $(run) systemctl stop $* ||:; \
	   $(run) systemctl disable $* ||: ; \
	   $(run) rm -rf $(basename $(SYSCONFDIR)/$*) $(dir $(SERVICE_OVERRIDE_FILE)) $(SERVICE_TIMER_FILE); \
	   $(run) systemctl daemon-reload; \
	)

#//
$(addsuffix .service/name,$(SERVICE_PREFIX)$(SERVICE)): %/name: FORCE
	@echo $*

$(addsuffix .service/status,$(SERVICE_PREFIX)$(SERVICE)): %/status: FORCE
	@( set -x; $(run) systemctl status $* ||:; )

$(addsuffix .service/start,$(SERVICE_PREFIX)$(SERVICE)): %/start: FORCE
	@( set -x; $(run) systemctl start $* ||:; )

$(addsuffix .service/stop,$(SERVICE_PREFIX)$(SERVICE)): %/stop: FORCE
	@( set -x; $(run) systemctl stop $* ||:; )

$(addsuffix .service/enable,$(SERVICE_PREFIX)$(SERVICE)): %/enable: FORCE
	@( set -x; $(run) systemctl enable $* ||:; )

$(addsuffix .service/disable,$(SERVICE_PREFIX)$(SERVICE)): %/disable: FORCE
	@( set -x; $(run) systemctl disable $* ||:; )

#//
$(SYSCONFDIR)/%: FORCE
	@if [ ! -e $(basename $@) ]; then \
	  ( set -x; $(run) ln -sf $(PWD) $(basename $@) ); \
	fi

ifdef SERVICE_OVERRIDE_FILE
$(SERVICE_OVERRIDE_FILE): $(SERVICE_OVERRIDE)
	@( set -x; \
	   $(run) install -o $(INSTALL_OWNER) -g $(INSTALL_GROUP) -m 744 -d $(@D); \
	   $(run) install -o $(INSTALL_OWNER) -g $(INSTALL_GROUP) -m 644 $< $@; \
	)
endif

#//
ifdef SERVICE_TIMER_FILE
$(addsuffix .timer/status,$(SERVICE_PREFIX)$(SERVICE)): %/status: FORCE
	@( set -x; $(run) systemctl status $* ||:; )

$(addsuffix .timer/start,$(SERVICE_PREFIX)$(SERVICE)): %/start: FORCE
	@( set -x; $(run) systemctl start $* ||:; )

$(addsuffix .timer/stop,$(SERVICE_PREFIX)$(SERVICE)): %/stop: FORCE
	@( set -x; $(run) systemctl stop $* ||:; )

$(addsuffix .timer/enable,$(SERVICE_PREFIX)$(SERVICE)): %/enable: FORCE
	@( set -x; $(run) systemctl enable $* ||:; )

$(addsuffix .timer/disable,$(SERVICE_PREFIX)$(SERVICE)): %/disable: FORCE
	@( set -x; $(run) systemctl disable $* ||:; )

$(SERVICE_TIMER_FILE): $(SERVICE_TIMER)
	@( set -x; \
	   $(run) install -o $(INSTALL_OWNER) -g $(INSTALL_GROUP) -m 644 $< $@; \
	)
endif

#//
FORCE:
