class apt_mirror(
  Variant[String, Boolean] $ensure = present,
  Variant[String, Boolean] $service_ensure = running,
  Optional[Hash] $settings = {},
) inherits apt_mirror::params {
  notice('---')

  $app_settings = $apt_mirror::params::settings + std::nv($settings, Hash, {})
  info(std::pp({'app_settings' => $app_settings}))

  $file_ensure = std::file_ensure($ensure)
  $dir_ensure = std::dir_ensure($ensure)

  contain apt_mirror::install
  contain apt_mirror::config
  contain apt_mirror::service

  case $ensure {
    'absent', false: {
      # absent の場合は disable service を先に行う
      Class[apt_mirror::service]
      -> Class[apt_mirror::install]
      -> Class[apt_mirror::config]
    }
    default: {
      Class[apt_mirror::install]
      -> Class[apt_mirror::config]
      -> Class[apt_mirror::service]
    }
  }
}

class apt_mirror::install {
  #// config files
  file { 'apt_mirror::config':
    path => "${apt_mirror::app_dir}/config",
    ensure => $apt_mirror::dir_ensure,
    force => true,
    require => File[apt_mirror::app_dir],
  }

  file { 'apt_mirror::config::mirror_list':
    path => "${apt_mirror::app_dir}/config/mirror.list",
    ensure => $apt_mirror::file_ensure,
    content => epp('apt_mirror/mirror.list', $apt_mirror::app_settings),
    require => File[apt_mirror::config],
  }

  #// spool directory
  file { 'apt_mirror::spool':
    path => "${apt_mirror::spool_dir}",
    ensure => directory,
    force => false,
    mode => '1775',             # drwxrwxr-t
    owner => 'root',
    group => 'wheel',
  }

  #// setup service
  stdapp::service::install { apt_mirror: }

  #// supplemental stuff
  file { 'apt_mirror::bin':
    path => "${apt_mirror::app_dir}/bin",
    ensure => $apt_mirror::dir_ensure,
    force => true,
    recurse => true,
    require => File[apt_mirror::app_dir],
    * => $apt_mirror::dir_ensure ? {
      'directory' => {source => 'puppet:///modules/apt_mirror/bin'},
      default => {},
    },
  }

  file { 'apt_mirror::data::spool':
    path => "${apt_mirror::app_dir}/data.apt-mirror",
    ensure => $apt_mirror::file_ensure ? { 'file' => 'link', default => 'absent' },
    target => "${apt_mirror::spool_dir}/mirror/${apt_mirror::app_settings[mirror_domain]}",
    require => File[apt_mirror::app_dir],
  }
}

class apt_mirror::config {
  #//empty
}

class apt_mirror::service {
  stdapp::service { apt_mirror: }
}
