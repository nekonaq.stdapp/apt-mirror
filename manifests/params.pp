class apt_mirror::params {
  include stdapp

  $app_dir = "${stdapp::base_dir}/apt-mirror"
  $service_prefix = 'docker-compose@apt-mirror'
  $service_override = 'apt_mirror/service.override.conf'
  $service_timer = 'apt_mirror/service.timer'

  $value = parseyaml(file('apt_mirror/params.yml'))
  $settings = std::nv($value['apt_mirror::settings'], Hash, {})

  $spool_dir = $settings['spool_dir']
}
